#!/usr/bin/python3.6
### Import lib
import sys
from pymongo import MongoClient
from collections import defaultdict
from Util import ReviewerFunctions
from Class import ReviewerClass
from Util import commentFilterFunctions
import datetime
import numpy as np
import csv
import re
from csv import DictReader
from dateutil.relativedelta import relativedelta
from datetime import datetime as dt

### Class
class developer():
    def __init__(self):
        self.authorExp = 0
        self.reviewerExp = 0
        self.agedAuthorExp = defaultdict(lambda: [])
        self.agedReviewerExp = defaultdict(lambda: [])
        self.priorDirectoryAuthored = defaultdict(lambda: 0)
        self.priorDirectoryReviewed = defaultdict(lambda: 0)
    def addAuthorExp(self):
        self.authorExp = self.authorExp + 1
    def addReviewerExp(self):
        self.reviewerExp = self.reviewerExp + 1

class recordRevision():
    def __init__(self, revNum, createdTime, purpose):
        self.revNum = revNum
        self.createdTime = createdTime
        self.files = []
        self.purpose = purpose
    def recordModifiedFile(self, fileName):
        self.files.append(fileName)

class fileEntity():
    def __init__(self):
        self.authorIdList = []
        self.createdTimeList = []
        self.numDefects = []
        self.discussionLen = []
        self.feedbackDelay = []
        self.numReviewers = []

def getPurpose(description):
    ### Identify Patch purpose
    purpose = "Feature"
    # Document keywordss
    doc = re.compile(r'doc')
    copyright = re.compile(r'copyright')
    license = re.compile(r'license')
    # Bug fixing keywords
    bug = re.compile(r'bug')
    fix = re.compile(r'fix')
    defect = re.compile(r'defect')
    if (re.search(doc, description) != None
        or re.search(copyright, description) != None
        or re.search(license, description) != None):
        purpose = "Document"
    if (re.search(bug, description) != None
        or re.search(fix, description) != None
        or re.search(defect, description) != None):
        purpose = "Bug"
    return purpose

# Define function
def calcRecentExp(dic, currentYearMonth):
    recentExp = 0
    for i in range(1, 7):
        recentExp += len(set(dic[currentYearMonth+relativedelta(months=-i)]))
    return recentExp

### Setting agrv
targetCommunity = sys.argv[1]
mongoClient = MongoClient('localhost')
db = mongoClient[targetCommunity]
col_reviews = db['reviews']
db_reviews = col_reviews.find(
    { '$or' : [{'status': 'MERGED'}, {'status': 'ABANDONED'}]}).sort("_number")
col_comments = db['comments']
db_comments = col_comments.find()
col_inlines = db['inlines']
db_inlines = col_inlines.find()

### Import bot ids from config file
botIdFile = './Config/'+targetCommunity+'_botId.csv'
botId = []
botIdFp = open(botIdFile, "rU")
for row in botIdFp:
    v = row.strip().split(",")
    assert (len(v) == 2)
    botId.append(int(v[0]))

### Import baseMetrics file
baseMetrics_file = '../data/metricsData/baseMetrics_mongo_'+targetCommunity+'.csv'
header = True
basicReviewInfo_dic = defaultdict(lambda: {'discussionLength': 0, 'numParticipations': 0})
with open(baseMetrics_file, 'r') as csvfile:
    reader = DictReader(csvfile, lineterminator='\n')
    for r in reader:
        basicReviewInfo_dic[int(r["changeIdNum"])]['discussionLength'] = int(r["discussionLength"])
        basicReviewInfo_dic[int(r["changeIdNum"])]['numParticipations'] = int(r["numParticipations"])

### Record db_comments and db_inlines
db_comments_list = defaultdict(lambda: None)
db_inlines_list = defaultdict(lambda: [])
for r in db_comments:
    db_comments_list[r['_number']] = r # Some have two elements but they are duplicated. It must be one.
for r in db_inlines:
    db_inlines_list[r['_number']].append(r)

print(len(db_comments_list.keys()))
print(len(db_inlines_list.keys()))

### Init values
developer_dic = defaultdict(lambda: 0)
t_revisionDic = defaultdict(lambda: None)
fileNameDic = defaultdict(lambda: None)
createdTime_dic = defaultdict(lambda: {'idList': [], 'pathList': []})
FMT = '%Y-%m-%d'

output = []
for i, row in enumerate(db_reviews, start=1):
    if i % 20000 == 1: print('Starting with %s' % i)
    if int(row["_number"]) not in basicReviewInfo_dic.keys():
        continue

    # Record revision info
    ch_changeIdNum = int(row["_number"])
    description = ''
    purpose = ''
    entropy_dic = defaultdict(lambda:None)
    fileList = []
    dirList = []
    subsystemList = []
    insersion = 0
    deletion = 0
    loc = 0
    churn = 0
    numFiles = 0
    entropy = 0.0
    for revEle in row['revisions']:
        #TODO: select initial revision
        if ("commit" in row['revisions'][revEle].keys()) and ("message" in row['revisions'][revEle]["commit"].keys()):
            description = row['revisions'][revEle]["commit"]["message"]
        else:
            pass
        purpose = getPurpose(description)
        t_revisionDic[ch_changeIdNum] = recordRevision(
            int(row['revisions'][revEle]['_number']),
            dt.strptime(row['created'].split('.')[0].split(' ')[0], '%Y-%m-%d'),
            purpose
        )
        # Record modified file information
        if 'files' not in row['revisions'][revEle].keys(): continue
        for fileName in row['revisions'][revEle]['files']:
            modifiedTypes = row['revisions'][revEle]['files'][fileName].keys()
            if "lines_inserted" in modifiedTypes:
                insersion += int(row['revisions'][revEle]['files'][fileName]["lines_inserted"])
            if "lines_deleted" in modifiedTypes:
                deletion += int(row['revisions'][revEle]['files'][fileName]["lines_deleted"])
            if "lines_inserted" in modifiedTypes or "lines_deleted" in modifiedTypes:
                t_revisionDic[ch_changeIdNum].recordModifiedFile(fileName)
            ###
            fpath = fileName
            dirName = fpath.split('/')
            subsystem = dirName[0]
            dirName.pop()
            dpath = '/'.join(dirName)
            if dpath not in dirList:
                dirList.append(dpath)
            if subsystem not in subsystemList:
                subsystemList.append(subsystem)
            assert(entropy_dic[fpath] == None)
            entropy_dic[fpath] = insersion + deletion
        ### Churn [Shane et al, EMSE2016]
        churn = insersion + deletion
        ### Entropy [Kamei et al, TSE2013]
        entropy = 0.0
        for k, v in entropy_dic.items():
            assert(type(v) == int)
            if churn > 0 and v > 0:
                entropy = entropy + (-1 * ((float(v)/float(churn)) * np.log2(float(v) / float(churn))))
            else:
                assert(v == 0)
        ### The number of modified files
        numFiles = len(t_revisionDic[ch_changeIdNum].files)
        ######
        break
    if t_revisionDic[ch_changeIdNum] == None:
        continue
    # Workload metrcis
    createdTime_dic[t_revisionDic[ch_changeIdNum].createdTime]['idList'].append(ch_changeIdNum)
    createdTime_dic[t_revisionDic[ch_changeIdNum].createdTime]['pathList'] += dirList
    overallCt = 0
    directoryCt = 0
    for d in range(1, 8):   # within 1 week
        assert(1 <= d and d <= 7)
        pastTime = t_revisionDic[ch_changeIdNum].createdTime - datetime.timedelta(days=d)
        # Overall workload
        overallCt += len(createdTime_dic[pastTime]['idList'])
        # Directory workload
        modifiedFlg = 0
        for dir in dirList:
            if (dir in createdTime_dic[pastTime]['pathList']):
                modifiedFlg = 1
        directoryCt += modifiedFlg

    # Record history info
    ch_authorAccountId = 0 if '_account_id' not in row['owner'].keys() else row['owner']['_account_id']
    ch_createdTime = dt.strptime(row['created'].split('.')[0].split(' ')[0], '%Y-%m-%d')
    ch_createdYearMonth = dt.strptime('-'.join(row['created'].split('.')[0].split(' ')[0].split('-')[0:2]), '%Y-%m')
    participants_list = []
    firstComment_CreatedTime = dt.strptime('3000-01-01', '%Y-%m-%d')
    for row in db_comments_list[ch_changeIdNum]['messages']:
        if ('author' in row.keys()) and ('_account_id' in row['author'].keys()):
            hist_message = row['message']
            hist_reviewer = row['author']['_account_id']
            hist_createdTime = dt.strptime(row['date'].split('.')[0].split(' ')[0], '%Y-%m-%d')
            if (hist_createdTime < firstComment_CreatedTime):
                firstComment_CreatedTime = hist_createdTime
            # Skipping a bot comment
            if hist_reviewer in botId:
                continue
            # Skipping a build comment
            if commentFilterFunctions.isBuildComment(hist_message) != None:
                continue
            # Counting participants
            if (hist_reviewer != ch_authorAccountId) and (hist_reviewer not in participants_list) and (hist_reviewer != ""):
                participants_list.append(hist_reviewer)
        else:
            pass

    ### Output & Updata total # authors & the last modification time & the last author's experience
    last_modification = 0
    curTime = ch_createdTime
    lastTime = curTime # the difference is 0
    last_modification = lastTime - curTime
    total_authors = 0
    numDefects = 0
    priorNumReviewers = []
    priorDiscussionLength = []
    priorfeedbackDelay = []
    for file in t_revisionDic[ch_changeIdNum].files:
        if fileNameDic[file] == None:
            fileNameDic[file] = fileEntity()
        assert(fileNameDic[file] != None)
        numDefects += len(fileNameDic[file].numDefects)
        priorDiscussionLength += fileNameDic[file].discussionLen
        priorNumReviewers += fileNameDic[file].numReviewers
        priorfeedbackDelay += fileNameDic[file].feedbackDelay
        ###
        if purpose == 'Bug': fileNameDic[file].numDefects.append(ch_changeIdNum)
        fileNameDic[file].discussionLen.append(basicReviewInfo_dic[ch_changeIdNum]['discussionLength'])
        fileNameDic[file].numReviewers.append(basicReviewInfo_dic[ch_changeIdNum]['numParticipations'])
        fileNameDic[file].feedbackDelay.append(firstComment_CreatedTime-ch_createdTime)
        ###
        if len(fileNameDic[file].createdTimeList) > 0:
            lastTime =  fileNameDic[file].createdTimeList[0]
        assert(type(curTime) == datetime.datetime and type(lastTime) == datetime.datetime)
        last_modification = curTime - lastTime
        total_authors += len(fileNameDic[file].authorIdList)
        if ch_authorAccountId not in fileNameDic[file].authorIdList:
            fileNameDic[file].authorIdList.insert(0, ch_authorAccountId)
        fileNameDic[file].createdTimeList.insert(0, ch_createdTime)
        #TODO: do sort when querying mongo
        sorted(fileNameDic[file].createdTimeList)

    ### Output & Updating author experience and author timely one.
    if developer_dic[ch_authorAccountId] == 0:
        developer_dic[ch_authorAccountId] = developer()
        assert(developer_dic[ch_authorAccountId] != 0)
    # Output author experiences and author timely one.
    authorExp = developer_dic[ch_authorAccountId].authorExp
    # Updating author experience and author timely one.
    developer_dic[ch_authorAccountId].addAuthorExp()
    developer_dic[ch_authorAccountId].agedAuthorExp[ch_createdYearMonth].append(ch_changeIdNum)
    ###  Recent author exp
    recentAuthorExp = 0
    recentAuthorExp = calcRecentExp(developer_dic[ch_authorAccountId].agedAuthorExp, ch_createdYearMonth)

    revExpList = []
    recentReviewerExp = 0
    ### Output & Updating reviewer experience.
    for rev in participants_list:
        if developer_dic[rev] == 0:
            developer_dic[rev] = developer()
        assert(developer_dic[rev] != 0)
        # Output reviewer experiences and reviewer timely experiences
        revExpList.append(developer_dic[rev].reviewerExp)
        # Updating reviewer experience and reviewer timely experiences
        developer_dic[rev].addReviewerExp()
        developer_dic[rev].agedReviewerExp[ch_createdYearMonth].append(ch_changeIdNum)
        # Caluclate recent rev exp
        recentReviewerExp += calcRecentExp(developer_dic[rev].agedReviewerExp, ch_createdYearMonth)

    ### Get and Update prior directory patches of author and reviewers.
    priorDirPatchofAuthor = 0
    priorDirPatchofReviewers = 0
    for directryPath in set(dirList):
        priorDirPatchofAuthor += developer_dic[ch_authorAccountId].priorDirectoryReviewed[directryPath]
        developer_dic[ch_authorAccountId].priorDirectoryAuthored[directryPath] += 1
        for participant in participants_list:
            priorDirPatchofReviewers += developer_dic[participant].priorDirectoryReviewed[directryPath]
            developer_dic[participant].priorDirectoryReviewed[directryPath] += 1

    ### Output
    revExpList = sorted(revExpList)
    if len(revExpList) == 0: revExpList.append(0)
    output += [[ch_changeIdNum, authorExp, sum(revExpList), max(revExpList),
                np.median(revExpList), min(revExpList), recentReviewerExp, priorDirPatchofReviewers,
                last_modification.days, total_authors, recentAuthorExp, priorDirPatchofAuthor,
                np.median(numDefects), np.median(priorNumReviewers), np.median(priorDiscussionLength),
                np.median(priorfeedbackDelay), '_'.join(str(x) for x in participants_list),
                churn, numFiles, len(dirList), len(subsystemList),
                len(description.split()), entropy, purpose,
                overallCt, directoryCt]]

### Export output
exportFileName = '../data/metricsData/dev_product_workloadMetrics_mongo_'+targetCommunity+'.csv'
print(exportFileName)
with open(exportFileName, 'w') as csvfile:
    writer = csv.writer(csvfile, lineterminator='\n')
    writer.writerow(["changeIdNum","authorExp","revsExp","maxRevExp",
                     "medRevExp","minRevExp", "recentReviewerExp", "priorDirPatchofReviewers",
                     "lastModificationDays","totalAuthors", "recentAuthorExp", "priorDirPatchofAuthor",
                     "numDefects", "priorNumReviewers", "priorDiscussionLength",
                     "priorfeedbackDelay", "participants_list",
                     "churn","numFiles","numDirectories", "numSubsystems",
                     "descriptionLength","entropy","purpose",
                     "overallWorkload", "directoryWorkload"])
    writer.writerows(output)
