### Import lib
import sys
import csv
from pymongo import MongoClient
import numpy
from collections import defaultdict
from Util import ReviewerFunctions
from Class import ReviewerClass
from Util import commentFilterFunctions
import datetime
from datetime import datetime as dt

### Set mongo config
targetCommunity = sys.argv[1]
mongoClient = MongoClient('localhost')
db = mongoClient[targetCommunity]
col_reviews = db['reviews']
db_reviews = col_reviews.find({ '$or' : [{'status': 'MERGED'}, {'status': 'ABANDONED'}]}).sort("_number")
col_comments = db['comments']
db_comments = col_comments.find()
col_inlines = db['inlines']
db_inlines = col_inlines.find()

### Import bot ids from config file
botIdFile = './Config/'+targetCommunity+'_botId.csv'
botId = []
botIdFp = open(botIdFile, "rU")
for row in botIdFp:
    v = row.strip().split(",")
    assert (len(v) == 2)
    botId.append(int(v[0]))
print('Bot Ids were recorded.')

### Record db_comments and db_inlines
db_comments_list = defaultdict(lambda: None)
db_inlines_list = defaultdict(lambda: [])
for r in db_comments:
    db_comments_list[r['_number']] = r # Some have two elements but they are duplicated. It must be one.
for r in db_inlines:
    db_inlines_list[r['_number']].append(r)
print('Recorded db_comments and db_inlines.')

### Init values
datetime_list = defaultdict(lambda: 0)
reviews_list = defaultdict(lambda: None)
FMT = '%Y-%m-%d %H:%M:%S'

# CSV header
all_developers_list = []
disLenList = []
numIteList = []
ct = 0
output = []
for i, row in enumerate(db_reviews):
    if i % 20000 == 1: print('Starting with %s' % i)
    ch_changeIdNum = row['_number']
    ch_changeId = row['change_id']
    ch_status = 0 if row['status'] == "MERGED" else  1
    ch_project = row['project']
    ch_patchAuthor = 0 if '_account_id' not in row['owner'].keys() else row['owner']['_account_id']
    t_list = []
    all_developers_list.append(int(ch_patchAuthor))

    # 'created' occationally becomes later than the others.
    if 'created' in row.keys():
        t_list.append(dt.strptime(row['created'].split('.')[0], '%Y-%m-%d %H:%M:%S'))
    if 'updated' in row.keys():
        t_list.append(dt.strptime(row['updated'].split('.')[0], '%Y-%m-%d %H:%M:%S'))
    if 'submitted' in row.keys():
        t_list.append(dt.strptime(row['submitted'].split('.')[0], '%Y-%m-%d %H:%M:%S'))
    ch_createdTime = sorted(t_list)[0]

    # Skipping a bot author patch
    if ch_patchAuthor in botId:
        continue

    participants_list = [] # The number of participants in the review
    score = 0
    discussionLen = 0
    authorComments_ct = 0
    general_ct = 0
    numIteration = 0
    revLen = 0
    revision_number = 1
    hist_createdTime = dt.strptime('2020-01-01 00:00:00', '%Y-%m-%d %H:%M:%S') # To get no later than a possible date
    histTimeList = []
    # Iterations: The number of iterations [].
    # Discussion length: The number of non-automated comments [Tsay et al, ICSE2014].
    if db_comments_list[ch_changeIdNum] == None:
        continue
    ge_num_words = []
    for row in db_comments_list[ch_changeIdNum]['messages']:
        if ('author' in row.keys()) and ('_account_id' in row['author'].keys()):
            hist_message = row['message']
            hist_reviewer = row['author']['_account_id']
            all_developers_list.append(int(hist_reviewer))
            createdTime = dt.strptime(row['date'].split('.')[0], '%Y-%m-%d %H:%M:%S')
            histTimeList.append(createdTime)
            if ('_revision_number' in row.keys()) and (revision_number < row['_revision_number']):
                revision_number = row['_revision_number']
            # Skipping a bot comment
            if hist_reviewer in botId:
                continue
            # Skipping a build comment
            if commentFilterFunctions.isBuildComment(hist_message) != None:
                continue
            # Skipping 'Uploaded patch set' comment
            if ReviewerFunctions.IsUpdate(hist_message) != None:
                continue
            if ReviewerFunctions.JudgeDicisionMaking(hist_message) != 0:
                continue
            #
            if (createdTime < hist_createdTime):
                hist_createdTime = createdTime
            #
            if ReviewerFunctions.JudgeVoteScore(hist_message) != 0:
                hist_message = commentFilterFunctions.removeRecheck(id, hist_message)
                hist_message = commentFilterFunctions.removeScoreFormula(hist_message)
                hist_message = commentFilterFunctions.removeInlineComments(hist_message)
                if len(hist_message.strip().split(' ')) < 2:
                    continue

            # Consider an author's comment
            if ch_patchAuthor == hist_reviewer:
                authorComments_ct += 1
            ### Measuring 'Discussion length' and '#iterations'
            discussionLen += 1
            general_ct += 1
            ge_num_words.append(len(hist_message.split()))
            # Counting participants # The other reviewer sometimes updates the change
            if (hist_reviewer != ch_patchAuthor) and (hist_reviewer not in participants_list) and (hist_reviewer != ""): # hist_reviewer == "" when integrating the change
                participants_list.append(hist_reviewer)

    # Set hist_initialFeedbackTime and hist_lastFeedbackTime
    hist_initialFeedbackTime = 0
    feedbackDelay = 0
    if len(histTimeList) > 0:
        histTimeList = sorted(histTimeList)
        feedDelayDiff = histTimeList[0] - ch_createdTime
        feedbackDelay = float(feedDelayDiff.total_seconds() / 60 / 60 / 24)
    else:
        continue

    ### Count Inline comments
    inline_ct = 0
    in_num_words = []
    for inline_revision in db_inlines_list[ch_changeIdNum]:
        for filePath in inline_revision['inline_comments']:
            for row in inline_revision['inline_comments'][filePath]:
                if ('author' in row.keys()) and ('_account_id' in row['author'].keys()):
                    il_authorAccId = row['author']['_account_id']
                    all_developers_list.append(int(il_authorAccId))
                    # Skip an author's comment
                    if ch_patchAuthor == il_authorAccId:
                        continue
                    il_message = row['message'].strip().split(' ')
                    if len(il_message) > 1:  # Eliminate one-word inline (e.g., done)
                        discussionLen += 1
                        inline_ct += 1
                        in_num_words.append(len(il_message))
    ### Output
    timediff = hist_createdTime - ch_createdTime
    revLen = int(timediff.total_seconds() / 60 / 60 / 24)
    if (revLen >= 0):
        assert((general_ct+inline_ct) == discussionLen and discussionLen >= authorComments_ct)
        output += [[ch_changeId, ch_changeIdNum, ch_status, ch_createdTime,
                    discussionLen, revision_number, revLen,
                    len(participants_list), general_ct, inline_ct,
                    authorComments_ct, ch_project, feedbackDelay,
                    numpy.sum(ge_num_words), numpy.sum(in_num_words)]]

mongoClient.close()

print('Total developers that have participated in reveiws as author or reviewer: %s' % (len(set(all_developers_list))))
exportFileName = '../data/metricsData/baseMetrics_mongo_'+targetCommunity+'.csv'# sys.argv[3]
with open(exportFileName, 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator='\n')
        writer.writerow(["changeId","changeIdNum","status", "createdTime",
                         "discussionLength","numIteration","revLen",
                         "numParticipations","numGeneralComments","numInlineComments",
                         "authorComments_ct", "project", "feedbackDelay",
                         "numGeneralWords", "numInlineWords"])
        writer.writerows(output)
