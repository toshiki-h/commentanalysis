#/bin/tcsh

set targetSystem = $argv[1]

echo 'Conducting base metrics extraction for '${targetSystem}
python3.6 baseMetrics_mongo.py ${targetSystem}

echo 'Conducting optional metrics extraction for '${targetSystem}
python3.6 dev_product_workloadMetrics_mongo.py ${targetSystem}

echo 'Merging base and optional metrics extraction for '${targetSystem}
python3.6 mergeMetrics_mongo.py ${targetSystem}

echo 'Explore the number of words in each comment for '${targetSystem}
python3.6 numOfWords_analysis_mongo.py ${targetSystem}
