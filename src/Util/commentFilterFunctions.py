#!/usr/bin/python3
import re
import sys
import csv
import time
#import mysql.connector
from collections import defaultdict
from datetime import datetime
from Class import ReviewerClass

### Define Functions
# JudgeVoteScore(m)
# @m:message

def removeScoreFormula(msg):
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:( (\+|\-)*Code-Review(\+|\-)(1|2))( Workflow(\+|\-)(1|2))*( Works for me| Verified)*", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:(( Workflow(\+|\-)(1|2))|( Works for me| Verified))( Code-Review(\+|\-)(1|2))*", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:( Looks good to me, but someone else must approve|Sanity review passed)", '', msg)
    #t = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:( Code-Review(\+|\-)(1|2))( Workflow(\+|\-)(1|2))*( Works for me| Verified)*", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:( Workflow(\+|\-)(1|2))( Code-Review(\+|\-)(1|2))*", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]: No score", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:( Major sanity problems found| Doesn'*t seem to work)", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]: I would prefer that you didn'*t (submit|merge) this", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:( Do not merge| Do not submit| Fails)", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:", '', msg)
    return msg

def isUpdate(msg):
	return re.compile(r'Uploaded patch set [1-9]?[0-9]?[0-9]').match(msg)

def isBuildComment(msg):
    # TODO: should skip bot build comments here using regex. For example, 'Verified', 'works for me' and so on
    if ("Build failed" in msg) or ("Build Failed" in msg) or ("Build queued" in msg) or ("Build Started" in msg):
        return True
    if ("Build succeed" in msg) or ("Build succeeded" in msg) or ("Build Successful" in msg):
        return True
    if ("FAILURE" in msg) or ("SUCCESS" in msg):
        return True
    if ("This change depends on a change that failed to merge" in msg):
        return True

def isIntegrationComment(msg):
    # TODO to add "code review expired after 1 week of no activity after a negative review"
    if ("Change has been successfully merged" in msg) or ("Cherry PickedThis patchset was cherry picked" in msg) or ("Abandoned" in msg):
        return True

def removeInlineComments(msg):
    msg = re.sub(r"\([1-9]?[0-9]?[0-9]( inline)* comments*\)", '', msg)
    return msg

def removeUrl(msg):
    msg = re.sub(r"https://\S* ", ' ', msg)
    msg = re.sub(r"https://\S*( |.$)", ' ', msg)
    return msg

def removeCommonTerms(msg):
    msg = re.sub(r"recheck", '', msg)
    msg = re.sub(r"Restored", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9] was rebased", '', msg)
    return msg

def removeVotingComment(msg):
    msg = re.sub(r"Patch Set [1-9]*: .*\n\n", '', msg)
    return msg

def removeRecheck(id, msg):
    m = msg
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:  recheck\n", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:  check experimental\n", '', msg)
    msg = re.sub(r"Patch Set [1-9]?[0-9]?[0-9]:(\n\nrebased| was rebased)", '', msg)
    if m != msg:
        print("%s %s" % (id, m))
    return msg
