#!/usr/bin/python3
##################
# Author:Toshiki Hirao
# CreatedOn: 2016/08/27
# Summary: This program is to define Functions for reviewer information.
##################
import re
import sys
import csv
import time
import MySQLdb
from collections import defaultdict
from datetime import datetime
from Class import ReviewerClass

### Define Functions
# JudgeVoteScore(m)
# @m:message

def JudgeTagPattern(m): #
	designList = ["Shallow Fix", "Flawed Changes", "Side Effect"]
	implementation = ["Patch Size", "Portability", "Backward Compatibility", "Legal Issues", "Squashing Commits", "Branch Placement"]
	unclearIntention = ["Unclear Intention"]
	testing  = ["Test Coverage", "Test Failure"]

	withdrawal = ["Self-change", "Persuasion"]
	integration = ["Patch Dependency", "Blueprint", 'Release Schedule']

	if m in designList:
		return 0
	elif m in implementation:
		return 1
	elif m in unclearIntention:
		return 2
	elif m in testing:
		return 3
	elif m in withdrawal:
		return 4
	elif m in integration:
		return 5

	return -1
