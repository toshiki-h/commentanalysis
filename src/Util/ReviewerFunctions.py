#!/usr/bin/python3
##################
# Author:Toshiki Hirao
# CreatedOn: 2015/09/18
# Summary: This program is to define Functions for reviewer information.
##################
import re
import sys
import csv
import time
#import mysql.connector
from collections import defaultdict
from datetime import datetime
from Class import ReviewerClass

### Define Functions
# JudgeVoteScore(m)
# @m:message
######
# If Score is '+1' -> return 1
# If Score is '-1' -> return -1
# If Score isn't '+1' or '-1' -> return 0
######
def JudgeVoteScore(m): # (Regular expression)
    # Score +1
	weakPositiveVote = []
	weakPositiveVote.append(r'Patch Set [1-9]*: Looks good to me, but someone else must approve') # Qt # OpenStack
	#weakPositiveVote.append(r'Patch Set [1-9]*: Works for me') # OpenStack
	#weakPositiveVote.append(r'Patch Set [1-9]*: Verified')
	#weakPositiveVote.append(r'Patch Set [1-9]*: Sanity review passed') #
	weakPositiveVote.append(r'Patch Set [1-9]*: Code-Review\+1') # OpenStack
	#weakPositiveVote.append(r'Patch Set [1-9]*: Workflow\+1')
	for p in weakPositiveVote:
		if re.compile(p).match(m):
#			print p
			return 1

    # Score +2
	strongPositiveVote = []
	strongPositiveVote.append(r'Patch Set [1-9]*: Looks good to me, approved') # Qt # OpenStack
	strongPositiveVote.append(r'Patch Set [1-9]*: Looks good to me') # OpenStack
	strongPositiveVote.append(r'Patch Set [1-9]*: Code-Review\+2') # OpenStack
	strongPositiveVote.append(r'Patch Set [1-9]*: Workflow\+1 Code-Review\+2') # OpenStack
	for p in strongPositiveVote:
		if re.compile(p).match(m):
#			print p
			return 2

    # Score -1
	weakNegativeVote = []
	weakNegativeVote.append(r"Patch Set [1-9]*: I would prefer that you didn'*t submit this") # Qt # OpenStack
	#weakNegativeVote.append(r'Patch Set [1-9]*: Sanity problems found') #
	weakNegativeVote.append(r'Patch Set [1-9]*: Code-Review-1') # OpenStack
	#weakNegativeVote.append(r'Patch Set [1-9]*: Workflow-1')
	#weakNegativeVote.append(r"Patch Set [1-9]*: Doesn'*t seem to work")
	weakNegativeVote.append(r"Patch Set [1-9]*: I would prefer that you didn'*t merge this")# OpenStack
	weakNegativeVote.append(r"Patch Set [1-9]*: -Code-Review")# OpenStack
	#weakNegativeVote.append(r'Patch Set [1-9]*: No score')
	#weakNegativeVote.append(r'Patch Set [1-9]*: Major sanity problems found') #
	for p in weakNegativeVote:
		if re.compile(p).match(m):
#			print p
			return -1

    # Score -2
	strongNegativeVote = []
	strongNegativeVote.append(r'Patch Set [1-9]*: Do not merge') # OpenStack
	strongNegativeVote.append(r'Patch Set [1-9]*: Code-Review-2') # OpenStack
	strongNegativeVote.append(r'Patch Set [1-9]*: Do not submit') # Qt # OpenStack
	#strongNegativeVote.append(r'Patch Set [1-9]*: Fails') # OpenStack
	for p in strongNegativeVote:
		if re.compile(p).match(m):
#			print p
			return -2

    # Score 0
	"""
	p1.append(r'Patch Set [1-9]*: No score')
	if p1.match(m):
		return 0
	"""
	# No Score
	return 0

# Judge whether the comment is Dicision or not
# @m:message
def JudgeDicisionMaking(m):
	# merge
	if re.compile(r'Change has been successfully cherry-picked').match(m):
		return 2
	if re.compile(r'Change has been successfully merged').match(m):
		return 2

	# abandoned
	if re.compile(r'Abandoned').match(m):
		return -2

	# Not JudgeDicisionMaking
	return 0

def IsUpdate(m):
	if re.compile(r'Uploaded patch set [1-9]*').match(m):
		return True
	elif re.compile(r'Patch Set [1-9]*: Patch Set [1-9]* was rebased*').match(m):
		return True
	elif re.compile(r'Patch Set [1-9]*: Published edit on patch set [1-9]*').match(m):
		return True        
	else:
		return None

def IsReviewerClass(r, reviewer_class):
	assert r > 0
	if reviewer_class[r] != 0:
		return True
	else:
		#print "False"
		return False

def MakeReviewerClass(r, reviewer_class):
	assert r > 0 and reviewer_class[r] == 0
	reviewer_class[r] = ReviewerClass.Reviewer(r)
	#print type(ReviewerClass.Reviewer(r))
	return r

def IsCorrectVoting(r, s, judge):
	if (s > 0 and judge > 0) or (s < 0 and judge < 0):
		assert (s == 1 and judge == 2) or (s == -1 and judge == -2)
		return True
	else:
		assert (s == -1 and judge == 2) or (s == 1 and judge == -2)
		return False
