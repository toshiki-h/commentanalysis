### Import Library
import sys
from collections import OrderedDict, defaultdict
from csv import DictReader
import csv
import re
from pymongo import MongoClient
from Util import ReviewerFunctions
from Class import ReviewerClass
from datetime import datetime as dt
from decimal import *
import numpy as np

### Define class, dictionary, and function

targets = sys.argv[1:]
print(targets)

target_dic = defaultdict(lambda: defaultdict(lambda: {'totalComments': [], 'totalWords': []}))

for t in targets:
    with open('../data/metricsData/baseMetrics_mongo_'+t+'.csv', 'rU') as csvfile:
        reader = DictReader(csvfile, lineterminator='\n')
        for r in reader:
            if int(r['discussionLength']) < 1 or int(r['numParticipations']) < 1:
                continue
            year = r['createdTime'].split(' ')[0].split('-')[0]
            month = r['createdTime'].split(' ')[0].split('-')[1]
            createdTime = year+'-'+month
            target_dic[t][createdTime]['totalComments'].append(int(r['discussionLength'])/int(r['numParticipations']))
            target_dic[t][createdTime]['totalWords'].append((float(r['numGeneralWords'])+float(r['numInlineWords']))/int(r['numParticipations']))

output = []
for t in targets:
    totalComments_list = []
    totalWords_list = []
    for timeKey in sorted(target_dic[t].keys()):
        if timeKey.split('-')[1] in ["04", "08", "12"]: #
            if totalComments_list == []:
                output += [[timeKey+'-01', t,
                            0, 0,
                            0, 0]]
            else:
                output += [[timeKey+'-01', t,
                            np.median(totalComments_list), np.mean(totalComments_list),
                            np.median(totalWords_list), np.mean(totalWords_list)]]
            totalComments_list = []
            totalWords_list = []
        else:
            totalComments_list += target_dic[t][timeKey]['totalComments']
            totalWords_list += target_dic[t][timeKey]['totalWords']

with open("../data/metricsData/trendAnalysis_rq1.csv", 'w') as csvfile:
    writer = csv.writer(csvfile, lineterminator='\n')
    writer.writerow(["time", "project",
    "medTotalComments", "aveTotalComments",
    "medTotalWords", "aveTotalWords"])
    writer.writerows(output)
