### Import lib
import sys
import csv
from collections import defaultdict
from csv import DictReader

### Setting agrv
targetProject = sys.argv[1]
baseMetrics = '../data/metricsData/baseMetrics_mongo_'+targetProject+'.csv'
developerMetrics = '../data/metricsData/dev_product_workloadMetrics_mongo_'+targetProject+'.csv'

### Import base file
def readMetricInfo(fileName, typeKey):
    assert(typeKey in ['base', 'dev_pro_work'])
    with open(fileName, 'r') as csvfile:
        reader = DictReader(csvfile, lineterminator='\n')
        for r in reader:
            assert(type(r["changeIdNum"]) == str)
            mergedMetrics_dic[r["changeIdNum"]][typeKey] = r
        return reader.fieldnames

mergedMetrics_dic = defaultdict(lambda: {'base': {}, 'dev_pro_work': {}})
allMetricsColumnNames = readMetricInfo(baseMetrics, 'base') + readMetricInfo(developerMetrics, 'dev_pro_work')[1:]

output = []
for idKey in mergedMetrics_dic.keys():
    metricsCandidates = {}
    metricsCandidates.update(mergedMetrics_dic[idKey]['base'])
    metricsCandidates.update(mergedMetrics_dic[idKey]['dev_pro_work'])
    out_list = []
    if len(metricsCandidates) != len(allMetricsColumnNames):
        continue
    for k in allMetricsColumnNames:
        out_list += [metricsCandidates[k]]
    output += [out_list]
        
### Export output
exportFileName = '../data/metricsData/allMetrics_mongo_'+targetProject+'.csv'
with open(exportFileName, 'w') as csvfile:
    writer = csv.writer(csvfile, lineterminator='\n')
    writer.writerow(allMetricsColumnNames)
    writer.writerows(output)