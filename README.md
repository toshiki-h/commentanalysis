# Understanding Developer Commenting Trends in Code Reviews

Thank you for coming to our replication package page. We are willing to publicize our dataset and source codes that are used in this study. To replicate this study, please follow the instruction below.

### [Preparation] Install our environment and unzip our dataset
To replicate this study, here is the environment to be prepared with respect to our analyses:
- Python3.4
- Mongo
- R 3.5.1
After the instration, the dataset of our studied systems needs to be unziped into mongo database.
- Download the dataset tar file that includes data of five systems (https://www.dropbox.com/s/bavwjwkakir9qyn/dataset.tar?dl=0)
- mongorestore --gzip --archive=chromium_mongo.archive
- mongorestore --gzip --archive=aosp_mongo.archive
- mongorestore --gzip --archive=qt_mongo.archive
- mongorestore --gzip --archive=eclipse_mongo.archive
- mongorestore --gzip --archive=libreoffice_mongo.archive

### [Step 1] Extract features that this study requires.
- tcsh run.sh chromium
- tcsh run.sh aosp
- tcsh run.sh qt
- tcsh run.sh eclipse
- tcsh run.sh libreoffice
- python3.4 trendAnalysis_rq1.py chromium aosp qt eclipse libreoffice

### [Step 2] Plot figures for RQ1--RQ3
- cd ./r/
- Rscript rq1_ieice2019.R
- Rscript trendAnalysis_rq1.R
- Rscript linearRegression.R

